package com.wouter.emailvalidator;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.planonsoftware.platform.ux.v2.ISXContext;
import com.planonsoftware.platform.ux.v2.ISXDatabaseQuery;
import com.planonsoftware.platform.ux.v2.ISXResultSet;

public class EmailLogger {

    /**
     * log all eamils in the database. or something else, or whatever, or what
     * @param aContext the context.
     */
    public void execute(ISXContext aContext) {
        ObjectMapper objectMapper = new ObjectMapper();

        List<String> fields = new ArrayList<>();
        try (InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/index.json")) {
            JsonNode readValue = objectMapper.readValue(stream, JsonNode.class);
            fields = StreamSupport.stream(readValue.get("fields").spliterator(), false) //
                    .map(field -> field.textValue()) //
                    .collect(Collectors.toList()); //
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectNode root = objectMapper.createObjectNode();
        ArrayNode resultsArray = objectMapper.createArrayNode();

        ISXDatabaseQuery query = aContext.getPVDatabaseQuery(ListEmailQuery.class.getSimpleName());
        ISXResultSet results = query.executeAll();
        while (results.next()) {
            ObjectNode entry = root.objectNode();

            fields.forEach(field -> {
                entry.put(field, results.getString(field));
            });

            resultsArray.add(entry);
        }

        root.set("results", resultsArray);
        try {
            objectMapper.writeValue(System.out, root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
