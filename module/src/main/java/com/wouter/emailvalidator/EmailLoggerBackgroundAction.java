package com.wouter.emailvalidator;

import java.util.Map;

import com.planonsoftware.platform.ux.v2.ISXBackgroundActionExecuter;
import com.planonsoftware.platform.ux.v2.ISXContext;

public class EmailLoggerBackgroundAction implements ISXBackgroundActionExecuter
{

	@Override
	public void execute(Map<String, String> aExecuterParameters, ISXContext aContext) {
        System.out.println("Background action");
		new EmailLogger().execute(aContext);
	}
}
