package com.wouter.emailvalidator;

import com.planonsoftware.platform.querybuilder.v2.IPnQueryBuilder;
import com.planonsoftware.platform.querybuilder.v2.IPnQueryDefinition;

public class ListEmailQuery implements IPnQueryDefinition
{

	@Override
	public void create(IPnQueryBuilder aBuilder) {
		aBuilder.addSelectField("FirstName");
        aBuilder.addSelectField("LastName");
        aBuilder.addSelectField("Email");
	}

	@Override
	public String getBOName() {
		return "Person";
	}
}
