package com.wouter.emailvalidator;

import com.planonsoftware.platform.ux.v2.ISXBusinessTransactionExecuter;
import com.planonsoftware.platform.ux.v2.ISXContext;

public class EmailLoggerBusinessTransaction implements ISXBusinessTransactionExecuter {

    @Override
    public void execute(ISXContext aContext) {
        System.out.println("In business transaction");
        new EmailLogger().execute(aContext);
    }
}
