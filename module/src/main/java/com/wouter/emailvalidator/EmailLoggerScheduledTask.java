package com.wouter.emailvalidator;

import com.planonsoftware.platform.ux.v2.ISXContext;
import com.planonsoftware.platform.ux.v2.IScheduledTask;

public class EmailLoggerScheduledTask implements IScheduledTask
{

	@Override
	public void execute(ISXContext aSXContext, String aParameter) {
        System.out.println("Executing list emails");

        System.out.println("In task");
        new EmailLogger().execute(aSXContext);

        EmailLoggerBusinessTransaction transaction = new EmailLoggerBusinessTransaction();
		aSXContext.executeBusinessTransaction(transaction);

        aSXContext.queueBackgroundAction("list users", EmailLoggerBackgroundAction.class, null);
	}

	@Override
	public String getDescription() {
		return "logs all users in the database";
	}
}
