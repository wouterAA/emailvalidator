package com.wouter.emailvalidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.planonsoftware.platform.ux.v2.IBusinessRule;
import com.planonsoftware.platform.ux.v2.ISXBusinessObject;
import com.planonsoftware.platform.ux.v2.ISXContext;
import com.planonsoftware.platform.ux.v2.ISXField;

public class ValidateEmailBusinessRule implements IBusinessRule {

    private static final Pattern PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);

    private final int warningCode = 1;

    @Override
    public void execute(ISXBusinessObject aSXBO, ISXBusinessObject aOldSXBO, ISXContext aSXContext, String aParameter) {
        ISXField field = aSXBO.getFieldByName("Email");

        String value = field.getValueAsString();        
        boolean empty = field.isEmpty() || value == null || value.trim().isEmpty();
        boolean mandatory = field.getFieldDefinition().isMandatory();

        // when the field is NOT mandatory, and the field is empty just ignore it.
        if(empty && !mandatory) {
            return;
        }

        // otherwise we'll have to validate it.
        Matcher matcher = PATTERN.matcher(value);
        if(matcher.matches()) {
            // email is valid.
            return;
        }

        // email is NOT valid, show error.
        aSXContext.addError(this.warningCode, String.format("The supplied e-mail (%s) is NOT valid.", value));
    }

    @Override
    public String getDescription() {
        return "validates email addresses";
    }
}
